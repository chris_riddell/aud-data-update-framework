<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Redis;
use Loggy;
use App\Facades\EveryAction;

class UpdateChapter implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $firstName;

    protected $lastName;

    protected $email;

    protected $chapter;

    protected $vanId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->firstName = isset($data['firstName']) ? $data['firstName'] : null;
        $this->lastName = isset($data['lastName']) ? $data['lastName'] : null;
        $this->email = isset($data['email']) ? $data['email'] : null;
        $this->chapter = isset($data['chapter']) ? $data['chapter'] : null;
        $this->vanId = isset($data['vanId']) ? $data['vanId'] : null;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
         Redis::throttle('everyAction')->allow(1000)->every(3600)->then(function () {
            if (isset($this->vanId) && null !== $this->chapter) {
                $result = EveryAction::UpdateChapter($this->vanId, $this->chapter);
                Loggy::info('chapterUpdate','Updating assigned chapter ('.$this->chapter->ChapterCode.':'.$this->chapter->ChapterName.') for '.$this->vanId.': '.(isset($result->status) ? $result->status : 'OK'));
            } elseif (null !== $this->chapter) {
                $result = EveryAction::updateChapterByName($this->firstName, $this->lastName, $this->email, $this->chapter);
                Loggy::info('chapterUpdate','Updating assigned chapter ('.$this->chapter->ChapterCode.':'.$this->chapter->ChapterName.') for '.$this->vanId.': '.(isset($result->status) ? $result->status : 'OK'));
            } else {
                $result = EveryAction::UpdateChapter($this->vanId);
                Loggy::info('chapterUpdate','Nulling assigned chapter info for '.$this->vanId.': '.(isset($result->status) ? $result->status : 'OK'));
            }

        }, function () {
            return $this->release(10);
        });
    }
}
