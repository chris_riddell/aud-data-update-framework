<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use League\Csv\Reader;
use League\Csv\Statement;
use Illuminate\Support\Facades\Storage;
use App\Facades\AudubonCenter;
use App\Jobs\UpdateCenter;

class FileProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fileProcess';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process file and update EveryAction records';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (Storage::disk('local')->exists('incoming/updates.csv')) {
            $path = storage_path('app/incoming/updates.csv');
            $reader = Reader::createFromPath($path)->setHeaderOffset(0);
            $records = $reader->getRecords();
            foreach ($records as $offset => $record) {
                $center = AudubonCenter::getCenter($record['ZIP']);
                $centerData = ['vanId' => $record['VANID'], 'center' => $center];
                UpdateCenter::dispatch($centerData);
            }
            Storage::move('incoming/updates.csv', 'processed/'.date('Ymd\TH+i+s+u').'-updates.csv');
        }
    }
}
