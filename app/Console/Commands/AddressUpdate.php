<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Facades\AudubonChapter;
use App\Jobs\UpdateChapter;

class AddressUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trigger:addressUpdate {--mode=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Address update trigger task';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('mode') == 'catchup') {
            $results = DB::connection('pipeline')->select("SELECT a.VanID, a.Zip5
                FROM dbo.TSM_AU_ContactsAddresses as a
                LEFT JOIN dbo.TSM_AU_CustomFields as f on f.VanID = a.VanID
                JOIN dbo.TSM_AU_Contacts as c on c.VanID = a.VanID
                WHERE c.PersonCommitteeId = 56490 AND a.Zip5 IS NOT NULL AND (f.ChapterId IS NULL OR f.ChapterName IS NULL)");
            foreach ($results as $r) {
                $chapter = AudubonChapter::getChapter($r->Zip5);
                $modified[] = $r->DateModified;
                $chapterData = ['vanId' => $r->VanID, 'chapter' => $chapter];
            }
        } if ($this->option('mode') == 'test') {
            $lastRunResult = DB::table('tasks')->where('name', 'addressUpdate')->latest('last_modified')->first();
            $lastRun = $lastRunResult->last_modified;
            $results = DB::connection('pipeline')->select("SELECT a.VanID, a.Zip5, a.DateModified
                FROM dbo.TSM_AU_ContactsAddresses as a
                JOIN dbo.TSM_AU_Contacts as c on c.VanID = a.VanID
                WHERE CAST(a.DateModified as smalldatetime) > '$lastRun' AND c.VanID IN (100232604,100458462)");
            $modified = [];
            foreach ($results as $r) {
                $chapter = AudubonChapter::getChapter($r->Zip5);
                $modified[] = $r->DateModified;
                $chapterData = ['vanId' => $r->VanID, 'chapter' => $chapter];
                UpdateChapter::dispatch($chapterData);
            }
            if ($modified) {
                $lastModified = max($modified);
                DB::connection()->insert("INSERT INTO tasks (name, last_modified) VALUES ('addressUpdate','$lastModified')");
            }
        } else {
            $lastRunResult = DB::table('tasks')->where('name', 'addressUpdate')->latest('last_modified')->first();
            $lastRun = $lastRunResult->last_modified;
            $results = DB::connection('pipeline')->select("SELECT a.VanID, a.Zip5, a.DateModified
                FROM dbo.TSM_AU_ContactsAddresses as a
                JOIN dbo.TSM_AU_Contacts as c on c.VanID = a.VanID
                WHERE c.PersonCommitteeId = 56490 AND CAST(a.DateModified as smalldatetime) > '$lastRun'");
            $modified = [];
            foreach ($results as $r) {
                $chapter = AudubonChapter::getChapter($r->Zip5);
                $modified[] = $r->DateModified;
                $chapterData = ['vanId' => $r->VanID, 'chapter' => $chapter];
                UpdateChapter::dispatch($chapterData);
            }
            if ($modified) {
                $lastModified = max($modified);
                DB::connection()->insert("INSERT INTO tasks (name, last_modified) VALUES ('addressUpdate','$lastModified')");
            }
        }
    }
}
