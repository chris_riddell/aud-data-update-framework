<?php
namespace App\Providers;

use App\Clients\EveryActionClient;
use Illuminate\Support\ServiceProvider;

/**
 * A Laravel service provider that injects all of the EveryACtion API endpoints
 * into the service container.
 */
class EveryActionServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/everyaction.php', 'everyaction');

        $this->publishes([
            __DIR__ . '/../../config/everyaction.php' => config_path('everyaction.php'),
        ]);
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $app = $this->app;

        $app->bind('App\Clients\EveryAction', function () {
            return new EveryActionClient();
        });

        $app->alias('App\Clients\EveryAction', 'everyaction');
    }
}