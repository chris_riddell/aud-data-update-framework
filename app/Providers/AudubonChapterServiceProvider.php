<?php
namespace App\Providers;

use App\Clients\AudubonChapterClient;
use Illuminate\Support\ServiceProvider;

/**
 * A Laravel service provider that injects the center update client
 * into the service container.
 */
class AudubonChapterServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $app = $this->app;

        $app->bind('App\Clients\AudubonChapter', function () {
            return new AudubonChapterClient();
        });

        $app->alias('App\Clients\AudubonChapter', 'audubonchapter');
    }
}