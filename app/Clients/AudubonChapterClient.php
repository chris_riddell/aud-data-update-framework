<?php

namespace App\Clients;

use Illuminate\Support\Facades\DB;

class AudubonChapterClient {
  
  public function getChapter($zipCode) {
    $chapter = DB::connection('pipeline')->table('AUDPORTAL.ChapterReporting.dbo.Chapter_Zipcode as z')->select('c.*')->where('z.ZipCode', $zipCode)->join('AUDPORTAL.ChapterReporting.dbo.Chapter as c', 'z.ChapterCode', '=', 'c.ChapterCode')->first();
    return is_object($chapter) ? $chapter : null;
  }
}