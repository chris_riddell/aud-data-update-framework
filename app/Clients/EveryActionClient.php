<?php

namespace App\Clients;

use GuzzleHttp\Client as GuzzleHttpClient;

class EveryActionClient
{
  /**
   * @var GuzzleHttp API Client
   */
  protected $client;

  /**
   * Create new instance of GuzzleHttp
   */
  public function __construct() {
    $this->client = new GuzzleHttpClient([
        'timeout' => 20,
        'base_uri' => config('everyaction.api_base') . '/' . config('everyaction.api_version') . '/'
    ]);
  }

  /**
  * @param $endpoint
  * @param array $query
  * @return mixed
  */
  protected function makeRequest($endpoint, $op = 'POST', $data=[], $query=[]) {
    $apiRequest = $this->client->request($op, $endpoint, [
      'auth' => [config('everyaction.api_username'), config('everyaction.api_key')],
       \GuzzleHttp\RequestOptions::JSON => $data,
       'query' => $query
    ]);
    $response = $apiRequest->getBody()->getContents();
    $api_data = json_decode($response);
    if (isset($api_data->errors) && count($api_data->errors) > 0) {
      // @TODO: Handler Error
    } else {
      return $api_data;
    }
  }

  public function updateChapterByName($firstName, $lastName, $email, $chapter) {
    return $this->makeRequest('people/findOrCreate', 'POST', ['firstName' => $firstName, 'lastName' => $lastName, 'emails' => [['email' => $email, 'type' => 'P', 'isPreferred' => null, 'isSubscribed' => null]], 'customFieldValues' => [['customFieldId' => 517, 'customFieldGroupId' => 142, 'assignedValue' => $chapter->ChapterName]]]);
  }

  public function updateChapter($vanId, $chapter = null) {
      if ($chapter) {
        return $this->makeRequest('people/findOrCreate', 'POST', ['vanId' => $vanId, 'customFieldValues' => [['customFieldId' => 45, 'customFieldGroupId' => 11, 'assignedValue' => $chapter->ChapterName], ['customFieldId' => 46, 'customFieldGroupId' => 11, 'assignedValue' => $chapter->ChapterCode], ['customFieldId' => 47, 'customFieldGroupId' => 11, 'assignedValue' => $chapter->Website], ['customFieldId' => 48, 'customFieldGroupId' => 11, 'assignedValue' => $chapter->Email]]]);
      } else {
        return $this->makeRequest('people/findOrCreate', 'POST', ['vanId' => $vanId, 'customFieldValues' => [['customFieldId' => 45, 'customFieldGroupId' => 11, 'assignedValue' => ''], ['customFieldId' => 46, 'customFieldGroupId' => 11, 'assignedValue' => ''], ['customFieldId' => 47, 'customFieldGroupId' => 11, 'assignedValue' => ''], ['customFieldId' => 48, 'customFieldGroupId' => 11, 'assignedValue' => '']]]);
      }
  }

  public function findPerson($vanId) {
    return $this->makeRequest('people/find/'.$vanId, 'GET', null, ['expand' => 'phones,emails,addresses,customFields,externalIds,recordedAddresses,preferences,suppressions,reportedDemographics']);
  }

}