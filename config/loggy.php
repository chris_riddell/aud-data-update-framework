<?php

return [
    'channels' => [
        'chapterUpdate' => [
            'log' => 'chapterUpdate.log',
            'daily' => true,
            'level' => 'info'
        ],
    ]
];
