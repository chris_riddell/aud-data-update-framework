<?php
  return [
    'api_username' => env('EVERYACTION_API_USERNAME'),
    'api_key' => env('EVERYACTION_API_KEY'),
    'api_base' => env('EVERYACTION_API_BASE', 'https://api.securevan.com'),
    'api_version' => env('EVERYACTION_API_VERSION', 'v4')
  ];